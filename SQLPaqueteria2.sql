
-- Use Paqueteria database
IF DB_ID('Paqueteria') IS NULL
    CREATE DATABASE Paqueteria;
GO
USE Paqueteria;
GO

-- Table Paqueteria
IF OBJECT_ID('Paqueteria', 'U') IS NOT NULL
    DROP TABLE Paqueteria;
GO
CREATE TABLE Paqueteria (
  idPaqueteria INT NOT NULL PRIMARY KEY,
  nombre_paqueteria VARCHAR(45) NOT NULL,
  rfc_paqueteria VARCHAR(45) NOT NULL,
  telefono_paqueteria VARCHAR(45) NOT NULL,
  direccion_paqueteria VARCHAR(80) NOT NULL
);

-- Table Destinatario
IF OBJECT_ID('Destinatario', 'U') IS NOT NULL
    DROP TABLE Destinatario;
GO
CREATE TABLE Destinatario (
  idDestinatario INT NOT NULL PRIMARY KEY,
  nombre_destinatario VARCHAR(45) NOT NULL,
  apellido_destinatario VARCHAR(45) NOT NULL,
  direccion_destinatario VARCHAR(45) NOT NULL,
  telefono_destinatario VARCHAR(45) NOT NULL,
  correo_destinatario VARCHAR(45)
);

-- Table Remitente
IF OBJECT_ID('Remitente', 'U') IS NOT NULL
    DROP TABLE Remitente;
GO
CREATE TABLE Remitente (
  idRemitente INT NOT NULL,
  remitente_nombre VARCHAR(45) NOT NULL,
  remitente_apellido VARCHAR(45) NOT NULL,
  remitente_direccion VARCHAR(80) NOT NULL,
  remitente_telefono VARCHAR(45) NOT NULL,
  remitente_correoelectronico VARCHAR(45),
  Paqueteria_idPaqueteria INT NOT NULL,
  Destinatario_idDestinatario INT NOT NULL,
  PRIMARY KEY (idRemitente, Paqueteria_idPaqueteria, Destinatario_idDestinatario),
  FOREIGN KEY (Paqueteria_idPaqueteria) REFERENCES Paqueteria (idPaqueteria),
  FOREIGN KEY (Destinatario_idDestinatario) REFERENCES Destinatario (idDestinatario)
);

-- Table Almacenista
IF OBJECT_ID('Almacenista', 'U') IS NOT NULL
    DROP TABLE Almacenista;
GO
CREATE TABLE Almacenista (
  idAlmacenista INT NOT NULL PRIMARY KEY,
  nombre_almacenista VARCHAR(45) NOT NULL,
  apellido_almacenista VARCHAR(45) NOT NULL,
  telefono_almacenista VARCHAR(45) NOT NULL,
  edad INT,
  curp_almacenista VARCHAR(45) NOT NULL,
  genero VARCHAR(45)
);

-- Table Sucursal
IF OBJECT_ID('Sucursal', 'U') IS NOT NULL
    DROP TABLE Sucursal;
GO
CREATE TABLE Sucursal (
  idSucursal INT NOT NULL,
  direccion_sucursal VARCHAR(45) NOT NULL,
  telefono_sucursal VARCHAR(45) NOT NULL,
  correo_sucursal VARCHAR(45) NOT NULL,
  Paqueteria_idPaqueteria INT NOT NULL,
  PRIMARY KEY (idSucursal, Paqueteria_idPaqueteria),
  FOREIGN KEY (Paqueteria_idPaqueteria) REFERENCES Paqueteria (idPaqueteria)
);

-- Table Almacen
IF OBJECT_ID('Almacen', 'U') IS NOT NULL
    DROP TABLE Almacen;
GO
CREATE TABLE Almacen (
  idAlmacen INT NOT NULL,
  telefono_almacen VARCHAR(45) NOT NULL,
  Almacenista_idAlmacenista INT NOT NULL,
  Sucursal_idSucursal INT NOT NULL,
  Sucursal_Paqueteria_idPaqueteria INT NOT NULL,
  total_paquetes INT NOT NULL,

  PRIMARY KEY (idAlmacen, Almacenista_idAlmacenista, Sucursal_idSucursal, Sucursal_Paqueteria_idPaqueteria),
  FOREIGN KEY (Almacenista_idAlmacenista) REFERENCES Almacenista (idAlmacenista),
  FOREIGN KEY (Sucursal_idSucursal, Sucursal_Paqueteria_idPaqueteria) REFERENCES Sucursal (idSucursal, Paqueteria_idPaqueteria)
);

-- Table Seccion
IF OBJECT_ID('Seccion', 'U') IS NOT NULL
    DROP TABLE Seccion;
GO
CREATE TABLE Seccion (
 idSeccion INT NOT NULL,
  espacios_disponibles INT NOT NULL,
  Almacen_idAlmacen INT NOT NULL,
  Almacen_Almacenista_idAlmacenista INT NOT NULL,
  numero_paquetes INT NOT NULL,
  Sucursal_idSucursal INT NOT NULL,
  Sucursal_Paqueteria_idPaqueteria INT NOT NULL,
  PRIMARY KEY (idSeccion, Almacen_idAlmacen, Almacen_Almacenista_idAlmacenista, Sucursal_idSucursal, Sucursal_Paqueteria_idPaqueteria),
    FOREIGN KEY (Almacen_idAlmacen, Almacen_Almacenista_idAlmacenista, Sucursal_idSucursal, Sucursal_Paqueteria_idPaqueteria) 
        REFERENCES Almacen (idAlmacen, Almacenista_idAlmacenista, Sucursal_idSucursal, Sucursal_Paqueteria_idPaqueteria)
);

-- Table Lote
IF OBJECT_ID('Lote', 'U') IS NOT NULL
    DROP TABLE Lote;
GO
CREATE TABLE Lote (
  idLote INT NOT NULL PRIMARY KEY,
  peso_lote VARCHAR(45) NOT NULL,
  cantidad_paquetes INT NOT NULL
);

-- Table Busqueda
IF OBJECT_ID('Busqueda', 'U') IS NOT NULL
    DROP TABLE Busqueda;
GO
CREATE TABLE Busqueda (
  idBusqueda INT NOT NULL,
  fecha_busqueda DATE NOT NULL,
  busquedas_anteriores VARCHAR(120),
  Almacenista_idAlmacenista INT NOT NULL,
  PRIMARY KEY (idBusqueda, Almacenista_idAlmacenista),
  FOREIGN KEY (Almacenista_idAlmacenista) REFERENCES Almacenista (idAlmacenista)
);

-- Table Paquete
IF OBJECT_ID('Paquete', 'U') IS NOT NULL
    DROP TABLE Paquete;
GO
CREATE TABLE Paquete (
   idPaquete INT NOT NULL,
    peso FLOAT NOT NULL,
    altura FLOAT NOT NULL,
    anchura FLOAT NOT NULL,
    direccion_entrega VARCHAR(80) NOT NULL,
    estatus VARCHAR(45) NOT NULL,
    descripcion_paquete VARCHAR(60) NOT NULL,
    Seccion_idSeccion INT NOT NULL,
    Seccion_Almacen_idAlmacen INT NOT NULL,
    Seccion_Almacen_Almacenista_idAlmacenista INT NOT NULL,
    Seccion_Almacen_Sucursal_idSucursal INT NOT NULL,
    Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria INT NOT NULL,
    Lote_idLote INT NOT NULL,
    Remitente_idRemitente INT NOT NULL,
    Remitente_Paqueteria_idPaqueteria INT NOT NULL,
    Remitente_Destinatario_idDestinatario INT NOT NULL,
    Busqueda_idBusqueda INT NOT NULL,
    Busqueda_Almacenista_idAlmacenista INT NOT NULL,
    Destinatario_idDestinatario INT NOT NULL,
    PRIMARY KEY (idPaquete, 
	Seccion_idSeccion, 
	Seccion_Almacen_idAlmacen, 
	Seccion_Almacen_Almacenista_idAlmacenista, 
	Seccion_Almacen_Sucursal_idSucursal, 
	Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria),
    FOREIGN KEY (Seccion_idSeccion, 
	Seccion_Almacen_idAlmacen, 
	Seccion_Almacen_Almacenista_idAlmacenista, 
	Seccion_Almacen_Sucursal_idSucursal, 
	Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria)
        REFERENCES Seccion (idSeccion, Almacen_idAlmacen, Almacen_Almacenista_idAlmacenista, Sucursal_idSucursal, Sucursal_Paqueteria_idPaqueteria),
    FOREIGN KEY (Lote_idLote) REFERENCES Lote (idLote),
    FOREIGN KEY (Remitente_idRemitente, Remitente_Paqueteria_idPaqueteria, Remitente_Destinatario_idDestinatario) 
        REFERENCES Remitente (idRemitente, Paqueteria_idPaqueteria, Destinatario_idDestinatario),
    FOREIGN KEY (Busqueda_idBusqueda, Busqueda_Almacenista_idAlmacenista) REFERENCES Busqueda (idBusqueda, Almacenista_idAlmacenista),
    FOREIGN KEY (Destinatario_idDestinatario) REFERENCES Destinatario (idDestinatario)
);

-- Table Jefedelogistica
IF OBJECT_ID('Jefedelogistica', 'U') IS NOT NULL
    DROP TABLE Jefedelogistica;
GO
CREATE TABLE Jefedelogistica (
  idJefedelogistica INT NOT NULL PRIMARY KEY,
  nobre_logistica VARCHAR(45) NOT NULL,
  apellido_logistica VARCHAR(45) NOT NULL,
  telefono_logistica VARCHAR(45) NOT NULL,
  correo_logistica VARCHAR(45) NOT NULL,
  curp_logistica VARCHAR(45) NOT NULL,
  edad INT,
  genero VARCHAR(45)
);

-- Table Logistica
IF OBJECT_ID('Logistica', 'U') IS NOT NULL
    DROP TABLE Logistica;
GO
CREATE TABLE Logistica (
  idLogistica INT NOT NULL,
  municipio VARCHAR(45) NOT NULL,
  zona VARCHAR(45) NOT NULL,
  fecha_logistica DATE NOT NULL,
  hora_inicio TIME NOT NULL,
  hora_fin TIME NOT NULL,
  Jefedelogistica_idJefedelogistica INT NOT NULL,
  PRIMARY KEY (idLogistica, Jefedelogistica_idJefedelogistica),
  FOREIGN KEY (Jefedelogistica_idJefedelogistica) REFERENCES Jefedelogistica (idJefedelogistica)
);

-- Table HojaRuta
IF OBJECT_ID('HojaRuta', 'U') IS NOT NULL
    DROP TABLE HojaRuta;
GO
CREATE TABLE HojaRuta (
  idHojaRuta INT NOT NULL,
  direccion_inicio VARCHAR(45) NOT NULL,
  direccion_fin VARCHAR(45) NOT NULL,
  Logistica_idLogistica INT NOT NULL,
  Logistica_Jefedelogistica_idJefedelogistica INT NOT NULL,
  PRIMARY KEY (idHojaRuta, Logistica_idLogistica, Logistica_Jefedelogistica_idJefedelogistica),
  FOREIGN KEY (Logistica_idLogistica, Logistica_Jefedelogistica_idJefedelogistica) REFERENCES Logistica (idLogistica, Jefedelogistica_idJefedelogistica)
);

-- Table Vehiculo
IF OBJECT_ID('Vehiculo', 'U') IS NOT NULL
    DROP TABLE Vehiculo;
GO
CREATE TABLE Vehiculo (
  idVehiculo INT NOT NULL PRIMARY KEY,
  placas VARCHAR(45) NOT NULL,
  marca VARCHAR(45) NOT NULL,
  color VARCHAR(45) NOT NULL,
  capacidad_carga FLOAT NOT NULL,
  tipo_combustible VARCHAR(45) NOT NULL
);

-- Table Repartidor
IF OBJECT_ID('Repartidor', 'U') IS NOT NULL
    DROP TABLE Repartidor;
GO
CREATE TABLE Repartidor (
  idRepartidor INT NOT NULL PRIMARY KEY,
  nombre_repartidor VARCHAR(45) NOT NULL,
  apellido_repartidor VARCHAR(45) NOT NULL,
  telefono_repartidor VARCHAR(45) NOT NULL,
  correo_repartidor VARCHAR(45) NOT NULL,
  curp_repartidor VARCHAR(45) NOT NULL,
  edad INT,
  genero VARCHAR(45),
  Vehiculo_idVehiculo INT NOT NULL,
  FOREIGN KEY (Vehiculo_idVehiculo) REFERENCES Vehiculo (idVehiculo)
);

-- Table Guia
IF OBJECT_ID('Guia', 'U') IS NOT NULL
    DROP TABLE Guia;
GO
CREATE TABLE Guia (
 idGuia INT NOT NULL,
    descripcion_guia VARCHAR(60) NOT NULL,
    codigo_rastreo VARCHAR(45) NOT NULL,
    precio_guia FLOAT NOT NULL,
    Repartidor_idRepartidor INT NOT NULL,
    Paquete_idPaquete INT NOT NULL,
    Paquete_Seccion_idSeccion INT NOT NULL,
    Paquete_Seccion_Almacen_idAlmacen INT NOT NULL,
    Paquete_Seccion_Almacen_Almacenista_idAlmacenista INT NOT NULL,
    Paquete_Seccion_Almacen_Sucursal_idSucursal INT NOT NULL,
    Paquete_Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria INT NOT NULL,
    Paquete_Lote_idLote INT NOT NULL,
    Paquete_Remitente_idRemitente INT NOT NULL,
    Paquete_Remitente_Paqueteria_idPaqueteria INT NOT NULL,
    Paquete_Remitente_Destinatario_idDestinatario INT NOT NULL,
    Paquete_Busqueda_idBusqueda INT NOT NULL,
    Paquete_Busqueda_Almacenista_idAlmacenista INT NOT NULL,
    Paquete_Destinatario_idDestinatario INT NOT NULL,
    PRIMARY KEY (idGuia, 
	Repartidor_idRepartidor, 
	Paquete_idPaquete, 
	Paquete_Seccion_idSeccion, 
	Paquete_Seccion_Almacen_idAlmacen, 
	Paquete_Seccion_Almacen_Almacenista_idAlmacenista, 
	Paquete_Seccion_Almacen_Sucursal_idSucursal, 
	Paquete_Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria, 
	Paquete_Lote_idLote, 
	Paquete_Remitente_idRemitente, 
	Paquete_Remitente_Paqueteria_idPaqueteria, 
	Paquete_Remitente_Destinatario_idDestinatario, 
	Paquete_Busqueda_idBusqueda, 
	Paquete_Busqueda_Almacenista_idAlmacenista, 
	Paquete_Destinatario_idDestinatario),
    FOREIGN KEY (Repartidor_idRepartidor) REFERENCES Repartidor (idRepartidor),
    FOREIGN KEY (Paquete_idPaquete, 
	Paquete_Seccion_idSeccion, 
	Paquete_Seccion_Almacen_idAlmacen, 
	Paquete_Seccion_Almacen_Almacenista_idAlmacenista, 
	Paquete_Seccion_Almacen_Sucursal_idSucursal, 
	Paquete_Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria)
        REFERENCES Paquete (idPaquete, 
		Seccion_idSeccion, 
		Seccion_Almacen_idAlmacen, 
		Seccion_Almacen_Almacenista_idAlmacenista, 
		Seccion_Almacen_Sucursal_idSucursal, 
		Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria)
);

-- Table Ruta
IF OBJECT_ID('Ruta', 'U') IS NOT NULL
    DROP TABLE Ruta;
GO
CREATE TABLE Ruta (
  idRuta INT NOT NULL,
  direccion_inicio VARCHAR(45) NOT NULL,
  direccion_final VARCHAR(45) NOT NULL,
  Guia_idGuia INT NOT NULL,
  Guia_Repartidor_idRepartidor INT NOT NULL,
  Guia_Paquete_idPaquete INT NOT NULL,
  Guia_Paquete_Seccion_idSeccion INT NOT NULL,
  Guia_Paquete_Seccion_Almacen_idAlmacen INT NOT NULL,
  Guia_Paquete_Seccion_Almacen_Almacenista_idAlmacenista INT NOT NULL,
  Guia_Paquete_Seccion_Almacen_Sucursal_idSucursal INT NOT NULL,
  Guia_Paquete_Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria INT NOT NULL,
  Guia_Paquete_Lote_idLote INT NOT NULL,
  Guia_Paquete_Remitente_idRemitente INT NOT NULL,
  Guia_Paquete_Remitente_Paqueteria_idPaqueteria INT NOT NULL,
  Guia_Paquete_Remitente_Destinatario_idDestinatario INT NOT NULL, 
  Guia_Paquete_Busqueda_idBusqueda INT NOT NULL,
  Guia_Paquete_Busqueda_Almacenista_idAlmacenista INT NOT NULL,
  Guia_Paquete_Destinatario_idDestinatario INT NOT NULL,
  PRIMARY KEY (idRuta),

  FOREIGN KEY (Guia_idGuia, 
	Guia_Repartidor_idRepartidor, 
	Guia_Paquete_idPaquete, 
	Guia_Paquete_Seccion_idSeccion, 
	Guia_Paquete_Seccion_Almacen_idAlmacen, 
	Guia_Paquete_Seccion_Almacen_Almacenista_idAlmacenista, 
	Guia_Paquete_Seccion_Almacen_Sucursal_idSucursal, 
	Guia_Paquete_Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria, 
	Guia_Paquete_Lote_idLote, 
	Guia_Paquete_Remitente_idRemitente, 
	Guia_Paquete_Remitente_Paqueteria_idPaqueteria, 
	Guia_Paquete_Remitente_Destinatario_idDestinatario, 
	Guia_Paquete_Busqueda_idBusqueda, 
	Guia_Paquete_Busqueda_Almacenista_idAlmacenista, 
	Guia_Paquete_Destinatario_idDestinatario) 

  REFERENCES Guia (idGuia, 
	Repartidor_idRepartidor, 
	Paquete_idPaquete, 
	Paquete_Seccion_idSeccion, 
	Paquete_Seccion_Almacen_idAlmacen, 
	Paquete_Seccion_Almacen_Almacenista_idAlmacenista, 
	Paquete_Seccion_Almacen_Sucursal_idSucursal, 
	Paquete_Seccion_Almacen_Sucursal_Paqueteria_idPaqueteria, 
	Paquete_Lote_idLote, Paquete_Remitente_idRemitente, 
	Paquete_Remitente_Paqueteria_idPaqueteria, 
	Paquete_Remitente_Destinatario_idDestinatario, 
	Paquete_Busqueda_idBusqueda, 
	Paquete_Busqueda_Almacenista_idAlmacenista, 
	Paquete_Destinatario_idDestinatario)
);